import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  components: {
    gameBoard: false,
    gameIntro: false,
    gamePreloader: true,
    gameSettings: false
  }
}
const mutations = {
  changeComponent(state, name) {
    for (var property in state.components) {
      if (state.components.hasOwnProperty(property)) {
        if (property === name) {
          state.components[property] = true
        } else {
          state.components[property] = false
        }
      }
    }
  }
}

export default new Vuex.Store({
  state: state,
  mutations: mutations
})
